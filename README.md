# Flitter

### What is Flitter?
When I start this project it was set of tools for OpenBox desktop. Well, now things changed. Today's goal is create some replacement and improvment of XFCE desktop.

Am I crazy? Yes, I'm.

### Goal
So what your goal you ask? Things I want to create\support:

- Simple [File Manager](https://gitlab.com/flitter-tools/flitterfm) without useless functions as a [partily] replacement of Thunar. Why partily? Because I don't want and will not implement desktop of any kind.
- ~~App search dialog slash [dmenu](https://gitlab.com/flitter-tools/qmenu) as a replacement of xfce4-appfinder.~~ Decide to stick with standart XFCE dialog.
- Terminal emulator, I choose to fork QTerminal and make it look like [ST](https://gitlab.com/flitter-tools/qst).
- As we don't have desktop now, I write [simple wallpaper setter](https://gitlab.com/flitter-tools/flitter-wallpaper) based on hsetroot code.

Other things (not DE-related):

- [Menu editor](https://gitlab.com/flitter-tools/xdg-menu-edit) and desktop entries editor, as exo is too... Ah... Simple and feature less. As for menu editor: I can't find anything alive, ether it used python and never works on Arch, either is doesn't works at all on moder distribution.
- [Email client](https://gitlab.com/VolkMilit/CuteMail) because Thunderbird is full of shit to be email client, and other email clients are... Strange.
- [Goodies and patches to XFCE](https://gitlab.com/VolkMilit/xfce-hacks) self explainable.
- Libraies to communicate with [Freedesktop.org specifications](https://gitlab.com/flitter-tools/cutemime) and [XFCE](https://gitlab.com/flitter-tools/integration).
- Replacement of [Zenity](https://gitlab.com/flitter-tools/cutedialog), they're replace one widgets with another EVERY TIME. I mean, just look at color dialog... Like WTF?
- ~~Unify look of GTK and Qt app (well, we had Qt5 Style Plugins and it's works, but GTK3 doesn't think so...).~~ Found [this](https://github.com/krumelmonster/gtk3-mushrooms) but it's not active anymore, so I'm gonna fork it and keep it alive as match as possible.
- File Archiver, something we can share with FlitterFM (even maybe shared library).
- Replacement of catfish, because I found this programm frustrated. I think it should be part of FlitterFM.
- ~~Replacement of gsudo (isn't it dead after all?).~~ Doesn't needed anymore, we should stick with policykit, I mean [this one](https://github.com/ncopa/xfce-polkit). It works pretty well, tho not updated in a long time now.

### Philosophy
Follow [KISS](https://en.wikipedia.org/wiki/KISS_principle) \ [Less is more](https://en.wikipedia.org/wiki/Minimalism#Less_is_more_%28architecture%29) principles and [UNIX philosophy](https://en.wikipedia.org/wiki/Unix_philosophy) as XFCE (at least it claims so) do.

All dialogs must looks like one in XFCE, or at least be close to it.

Programms must follow XDG specifications, that's means at leat use QStandartPaths with Qt apps.

Use sh only for simple wrappers, not for complex programms.

### Stand alone programms
- ~~Viewnior as a replacement of Ristretto (Flitter integrates in it)~~ Viewnior is dead and probably will not survive when GTK2+ going died. Going to write my own, OR used [this](https://github.com/jurplel/qView)
- SpaceFM as a replacement of Thunar (don't use systemd shit, while FlitterFM is not ready...)
- SMPlayer as a replacement of Parole (also has MPRIS2 and can integrate to XFCE)
- DeadBeef as a replacement of Parole (also has MPRIS2 and can integrate to XFCE)
- ~~Leafpad as a replacement of Mousepad (don't use GTK3)~~ Mousepad has some nice features, and after shrooms patchset it looks like Leafpad, so I decide to give up on this one.
- PaleMoon as a Browser app (complicated Mozilla shit, but MoonChild is works on it and web is became complex, so...)
- Engrampa as a replacement of Roller. Yep, it can works without Mint and it works pretty well too!
